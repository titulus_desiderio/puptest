const puppeteer = require('puppeteer');

puppeteer.launch({
  headless: true,
  args: ['--no-sandbox', '--disable-setuid-sandbox'],
})
  .then(async browser => {
    console.log('launched')
    await browser.close();
  })
  .catch(e => {
    console.error('failed');
    throw e;
  });
